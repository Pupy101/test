## Install:
```linux
pip install -e .
```

---

## Modify audio (change gain/speed)
### __Need install ffmpeg__

Help:

```linux
modify --help
```

Example:
```linux
modify -i input.wav -o output.wav -c 5 -action gain
```

---

## Transcribate audio

Help:

```linux
transcribe --help
```

Example:
```linux
transcribe -a input.wav -t int8 -j output.json
```
