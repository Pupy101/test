import argparse
import json
import logging
from typing import Dict, List, Literal, Union, cast

from faster_whisper import WhisperModel


class TranscribateArgs(argparse.Namespace):  # pylint: disable=too-few-public-methods
    audio: str
    model: str
    device: str
    device_index: int
    compute_type: str
    beam_size: int


def inference(  # pylint: disable=too-many-arguments
    audio: str,
    model: str,
    device: Literal["cpu", "cuda"],
    device_index: int,
    compute_type: Literal["float32", "float16", "int8_float16", "int8"],
    beam_size: int,
) -> List[Dict[str, Union[float, str]]]:
    whisper = WhisperModel(model, device=device, device_index=device_index, compute_type=compute_type)
    segments, info = whisper.transcribe(audio, beam_size=beam_size)
    logging.warning("Detected language '%s' with probability %s", info.language, info.language_probability)
    return [{"start": _.start, "end": _.end, "text": _.text} for _ in segments]


def parse_args() -> TranscribateArgs:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-a", "--audio", type=str, required=True, help="Input audio path")
    parser.add_argument("-j", "--json-path", type=str, required=True, help="Path to output json")
    parser.add_argument(
        "-m", "--model", type=str, choices=["distil-large-v3", "large-v3"], default="distil-large-v3", help="Model name"
    )
    parser.add_argument(
        "-d", "--device", type=str, choices=["cpu", "cuda"], default="cpu", help="Device for inference whisper"
    )
    parser.add_argument("-i", "--device-index", type=int, default=0, help="Device index (only for cuda)")
    parser.add_argument(
        "-t",
        "--compute-type",
        type=str,
        choices=["float32", "float16", "int8_float16", "int8"],
        default="float32",
        help="Compute type",
    )
    parser.add_argument("-b", "--beam-size", type=int, default=2, help="Compute type")
    return cast(TranscribateArgs, parser.parse_args())


def main() -> None:
    args = parse_args()
    if args.device == "cpu":
        assert args.compute_type != "int8_float16", "For cpu not supported 'int8_float16' type"
    elif args.device == "cuda":
        assert args.compute_type != "int8", "For cuda not supported 'int8' type"
    kwargs = vars(args)
    output_path = kwargs.pop("json_path")
    transcription = inference(**vars(args))
    logging.warning("Transcription: %s", transcription)
    with open(output_path, mode="w", encoding="utf-8") as fp:
        json.dump(transcription, fp, ensure_ascii=False, indent=4)
