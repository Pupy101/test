import argparse
from os.path import splitext
from typing import cast

from pydub import AudioSegment
from pydub.effects import speedup

MIN_COEF = 0
MAX_COEF = 100


class ModifyArgs(argparse.Namespace):  # pylint: disable=too-few-public-methods
    input_audio: str
    output_audio: str
    action: str
    coefficient: float


def change_speed(audio: AudioSegment, coefficient: float) -> AudioSegment:
    return speedup(audio, playback_speed=coefficient)


def change_gain(audio: AudioSegment, coefficient: float) -> AudioSegment:
    return audio.apply_gain(coefficient)


def parse_args() -> ModifyArgs:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--input-audio", type=str, required=True, help="Input audio path")
    parser.add_argument("-o", "--output-audio", type=str, required=True, help="Output audio path")
    parser.add_argument("-a", "--action", type=str, default="gain", help="Action: 'speedup' or 'gain'")
    parser.add_argument("-c", "--coefficient", type=float, default=2, help="Coefficient for change")
    return cast(ModifyArgs, parser.parse_args())


def main() -> None:
    args = parse_args()
    assert MIN_COEF < args.coefficient < MAX_COEF, f"Coefficient must be in ({MIN_COEF}, {MAX_COEF})"
    assert args.action in {"speedup", "gain"}
    audio = AudioSegment.from_file(args.input_audio)
    if args.action == "speedup":
        changed_audio = change_speed(audio=audio, coefficient=args.coefficient)
    elif args.action == "gain":
        changed_audio = change_gain(audio=audio, coefficient=args.coefficient)
    else:
        raise RuntimeError(f"Strange action: '{args.action}'")
    _, extension = splitext(args.output_audio)
    changed_audio.export(args.output_audio, format=extension[1:])
