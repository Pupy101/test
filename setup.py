from pathlib import Path
from typing import List, Optional

from setuptools import find_packages, setup


def find_requirements(options: Optional[List[str]] = None) -> List[str]:
    options = options or []
    options.append("default")
    requirements: List[str] = []
    for option in options:
        requirements_file = Path(__file__).parent / "requirements" / f"{option}.txt"
        assert requirements_file.exists(), f"Not found file: {requirements_file}"
        with open(requirements_file, "r", encoding="utf-8") as file:
            for line in file:
                line = line.strip()
                if line:
                    requirements.append(line)
    return requirements


setup(
    name="aud",
    version="0.1",
    author="Sergei Porkhun",
    author_email="ser.porkhun41@gmail.com",
    packages=find_packages(),
    install_requires=find_requirements(),
    extras_require={"dev": find_requirements(["dev"])},
    entry_points={
        "console_scripts": [
            "modify = src.modify:main",
            "transcribe = src.transcribe:main",
        ]
    },
)
